<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is the pyrosound cloud model for PyroCMS
 *
 * @author Aat Karelse	
 * @website http://vuurrosmedia.nl
 * @package PyroSoundclouds
 * @subpackage 	PyroCMS
 */
class pyroscloud_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->_table = 'pyrosoundclouds';
	}
	
	//create a new item
	public function create($input)
	{
		$to_insert = array(
			'name' => $input['name'],
			'url' =>  $input['url'],
			'secrettoken' =>  $input['secrettoken']
		);

		return $this->db->insert('pyrosoundclouds', $to_insert);
	}

	//make sure the slug is valid
	public function _check_slug($slug)
	{
		$slug = strtolower($slug);
		$slug = preg_replace('/\s+/', '-', $slug);
		return $slug;
	}
	
	public function getnum($num)
	{

		


		return $this->db->where('url', $num)->get($this->_table)->result();
	}
	

}
