<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author      Aat Karelse
 * @website     http://vuurrosmedia.nl
 * @package     PyroCMS
 * @subpackage  pyrosoundclouds
 */
class Plugin_PyrosoundClouds extends Plugin
{

	private $modulename = 'pyrosoundclouds';
	private $themepath;
	private $modulepath;
	private $data;
	
	public function __construct()
	{
		$this->load->helper('file');
		$this->themepath = $this->load->get_var('template_views') . 'modules/' . $this->modulename . '/';
		
		if (get_file_info( ADDONPATH .'modules/' . $this->modulename . '/details.php' ))
		{
			$this->modulepath= ADDONPATH .'modules/' . $this->modulename;
		}
		else
		{
			$this->modulepath= SHARED_ADDONPATH .'modules/' . $this->modulename;
		}
	}


	function cloudlist()
	{
		$this->load->model('pyroscloud_m');
		$soundclouds = assoc_array_prop($this->pyroscloud_m->get_all());
		$torender = "<ul id='track-list-right'>";
		if (count($soundclouds))
		foreach ($soundclouds as $soundcloud)
		{
			if ($this->settings->playertype == "normal")
			{
				if ($soundcloud->secrettoken == '')
				{
					$torender .= "<li class='unlocked2'><object height='80px' width='100%'> <param name='movie' value='" . $this->settings->soundcloudurl . $soundcloud->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;buying=" . $this->settings->buyable . "&amp;show_comments=" . $this->settings->sound_comments . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always' src='" . $this->settings->soundcloudurl . $soundcloud->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "' type='application/x-shockwave-flash' width='100%'></object></li>";
				}
				else
				{
					$torender .= "<li class='unlocked2'><object height='80px'  width='100%'> <param name='movie' value='" . $this->settings->soundcloudurl . $soundcloud->url . "?secret_token=" . $soundcloud->secrettoken . "&amp;auto_play=false&amp;player_type=" . $soundcloud->playertype . "&amp;buying=" . $this->settings->buyable . "&amp;show_comments=" . $this->settings->sound_comments . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always' src='" . $this->settings->soundcloudurl . $soundcloud->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "' type='application/x-shockwave-flash' width='100%'></object></li>";
				}
			}
			else
			{
				if ($soundcloud->secrettoken == '')
				{
					$torender .= "<li class='unlocked2'><object height='20px'  width='100%'> <param name='movie' value='" . $this->settings->soundcloudurl . $soundcloud->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;&amp;color=" . $this->settings->backgroundcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always'  src='" . $this->settings->soundcloudurl . $soundcloud->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "' type='application/x-shockwave-flash' width='100%'></object></li>";
				}
				else 
				{
					$torender .= "<li class='unlocked2'><object height='20px' width='100%'> <param name='movie' value='" . $this->settings->soundcloudurl . $soundcloud->url . "?secret_token=" . $soundcloud->secrettoken . "&amp;auto_play=false&amp;player_type=" . $soundcloud->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always'  src='" . $this->settings->soundcloudurl . $soundcloud->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "' type='application/x-shockwave-flash' width='100%'></object></li>";
				}
			}
		}
		$torender .= "</ul>";
		return $torender;
	}

	function cloud()
	{
		$this->load->model('pyroscloud_m');
		
		$num = $this->attribute('num');
		
		$soundclouds = $this->pyroscloud_m->getnum($num);
		
		if (count($soundclouds))
		{
		if ($this->settings->playertype == "normal")
		{
			$torender = "<object height='80px' width='100%'>";
		}
		else
		{
			$torender = "<object height='20px' width='100%'>";
		}

		if ($soundclouds->secrettoken == '')
		{
			$torender .= "<param name='movie' value='" . $this->settings->soundcloudurl . $soundclouds->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always'  src='" . $this->settings->soundcloudurl . $soundclouds->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "' type='application/x-shockwave-flash' width='100%'></object>";
		}
		else 
		{
			$torender .= "<param name='movie' value='" . $this->settings->soundcloudurl . $soundclouds->url . "?secret_token=" . $soundclouds->secrettoken . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always'  src='" . $this->settings->soundcloudurl . $soundclouds->url . "&amp;auto_play=false&amp;player_type=" . $this->settings->playertype . "&amp;font=Arial&amp;color=" . $this->settings->backgroundcolor . "' type='application/x-shockwave-flash' width='100%'>";
		}
		}
		$torender .= "</object>";

		return $torender;
	}
	
	function sound()
	{
		$number = $this->attribute('number');
		$playertype = $this->attribute('playertype');
		$bgcolor = $this->attribute('backgroundcolor');
		$secrett = $this->attribute('secrettoken');
		
		if ($playertype == "normal")
		{
			$torender = "<object height='80px' width='100%'>";
		}
		else
		{
			$torender = "<object height='20px' width='100%'>";
		}

		if ($secrett == '')
		{
			return "<param name='movie' value='" . $this->settings->soundcloudurl . $number . "&amp;auto_play=false&amp;player_type=" . $playertype . "&amp;font=Arial&amp;color=" . $bgcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always'  src='" . $this->settings->soundcloudurl . $number . "&amp;auto_play=false&amp;player_type=" . $playertype . "&amp;font=Arial&amp;color=" . $bgcolor . "' type='application/x-shockwave-flash' width='100%'>";
		}
		else 
		{
			return "<param name='movie' value='" . $this->settings->soundcloudurl . $number . "?secret_token=" . $secrett . "&amp;auto_play=false&amp;player_type=" . $playertype . "&amp;font=Arial&amp;color=" . $bgcolor . "'> <param name='allowscriptaccess' value='always'> <param name='wmode' value='transparent'><embed wmode='transparent' allowscriptaccess='always' src='" . $this->settings->soundcloudurl . $number . "&amp;auto_play=false&amp;player_type=" . $playertype . "&amp;font=Arial&amp;color=" . $bgcolor . "' type='application/x-shockwave-flash' width='100%'>";
		}
	}



	function jssoundcloud()
	{
		$torender = "<script type='text/JavaScript'>" . $this->load->file($this->modulepath . '/js/soundclouddevkit.js',TRUE,TRUE) . "</script>";
		$data->modulename = $this->modulename;

		$torender .= "<script type='text/JavaScript'> SC.oEmbed('http://soundcloud.com/forss/sets/soulhack',  document.getElementById('soundcloudwidget')); </script>";
		$torender .= "<div id='soundcloudwidget'></div>";
		return $this->parser->parse_string($torender, $data, TRUE, TRUE);
		// return $torender;
	}

}

/* End of file plugin.php */
