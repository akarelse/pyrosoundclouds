<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * 
 * @author  	Aat Karelse
 * @author		PyroCMS Dev Team
 * @package		PyroCMS\Core\Widgets
 */
class Widget_PyrosoundClouds extends Widgets
{
public $title		= array(
		'en' => 'pyrosoundclouds',
	);
	public $description	= array(
		'en' => 'Show a list of soundcloud sounds'
	);
	public $author		= 'Aat Karelse';
	public $website		= 'http://vuurrosmedia.nl';
	public $version		= '1.0';

	public function run()
	{
		$this->load->model('pyroscloud_m');
		$soundclouds = $this->pyroscloud_m->get_all();

		if (count($soundclouds))
		{
			$items_exist = TRUE;
		}
		else
		{
			$items_exist = FALSE;
		}
		return array('soundsclouds' => $soundclouds,'items_exist' => $items_exist); 
	}	
	
	private function allencode($thearray)
	{
		$tempar = array();
		foreach($thearray as $k => $v)
		{
		$v->url = urlencode($v->url);
		$tempar[] = $v;
		}
		return $tempar;
	}
}
