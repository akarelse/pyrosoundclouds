<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is the pyrosoundclouds controller for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Pyrosoundclouds Module
 */
class PyrosoundClouds extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->library('firephp'); 
		// Load the required classes
		$this->load->model('pyroscloud_m');
		$this->lang->load('pyrosoundclouds');
		
		$this->template->append_css('module::pyrosoundclouds.css')
		->append_js('module::pyrosoundclouds.js');
	}

	/**
	 * All items
	 */
	public function index($num = 0)
	{

		if ($num == 0)
		{
		$data->items = $this->pyroscloud_m
		->get_all();
		}
		else
		{
		$num = $this->security->xss_clean($num);
		$data->items = $this->pyroscloud_m
		->getnum($num);
		
		}
			
		// we'll do a quick check here so we can tell tags whether there is data or not
		if (count($data->items))
		{
			$data->items_exist = TRUE;
		}
		else
		{
			$data->items_exist = FALSE;
		}
		$this->firephp->log($data->items);
		// we're using the pagination helper to do the pagination for us. Params are: (module/method, total count, limit, uri segment)
		// $data->pagination = create_pagination('pyrosoundclouds', count($data->items), 5, 2);

		$this->template->title($this->module_details['name'], 'the rest of the page title')
		->build('index', $data);
	}
}
