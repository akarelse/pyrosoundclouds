<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is the admin controller for pyrosoundclouds for PyroCMS
 *
 * @author 		Aat karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	PyroSoundclouds
 */
class Admin extends Admin_Controller
{
	protected $section = 'items';

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->load->model('pyroscloud_m');
		$this->load->library('form_validation');
		$this->lang->load('pyrosoundclouds');

		// Set the validation rules
		$this->item_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'url',
				'label' => 'Url',
				'rules' => 'trim|max_length[10]|required'
			),
			array(
				'field' => 'secrettoken',
				'label' => 'Secret Token',
				'rules' => 'trim|max_length[10]'
			)
		);

		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')
		->append_css('module::admin.css');
	}

	/**
	 * List all items
	 */
	public function index()
	{
		// here we fetch everything
		$items = $this->pyroscloud_m->get_all();

		// Build the view with.
		$this->template->title($this->module_details['name'])
		->set('items', $items)
		->build('admin/items');
	}

	public function create()
	{
		// Set the validation rules from the array above
		$this->form_validation->set_rules($this->item_validation_rules);

		// check if the form validation passed
		if($this->form_validation->run())
		{
			// See if the model can create the record
			if($this->pyroscloud_m->create($this->input->post()))
			{
				// All good...
				$this->session->set_flashdata('success', lang('pyrosoundclouds.success'));
				redirect('admin/pyrosoundclouds');
			}
			// Something went wrong. Show them an error
			else
			{
				$this->session->set_flashdata('error', lang('pyrosoundclouds.error'));
				redirect('admin/pyrosoundclouds/create');
			}
		}
		
		foreach ($this->item_validation_rules AS $rule)
		{
			$data->{$rule['field']} = $this->input->post($rule['field']);
		}

		// Build the view using soundclouds/views/admin/form.php
		$this->template->title($this->module_details['name'], lang('pyrosoundclouds.new_item'))
		->set($data)
		->build('admin/form');
	}
	
	public function edit($id = 0)
	{
		$this->data = $this->pyroscloud_m->get($id);

		// Set the validation rules from the array above
		$this->form_validation->set_rules($this->item_validation_rules);

		// check if the form validation passed
		if($this->form_validation->run())
		{
			// get rid of the btnAction item that tells us which button was clicked.
			// If we don't unset it MY_Model will try to insert it
			unset($_POST['btnAction']);
			
			// See if the model can create the record
			if($this->pyroscloud_m->update($id, $this->input->post()))
			{
				// All good...
				$this->session->set_flashdata('success', lang('sample.success'));
				redirect('admin/pyrosoundclouds');
			}
			// Something went wrong. Show them an error
			else
			{
				$this->session->set_flashdata('error', lang('pyrosoundclouds.error'));
				redirect('admin/pyrosoundclouds/create');
			}
		}

		// Build the view using sample/views/admin/form.php
		$this->template->title($this->module_details['name'], lang('pyrosoundclouds.edit'))
		->build('admin/form', $this->data);
	}
	
	public function delete($id = 0)
	{
		// make sure the button was clicked and that there is an array of ids
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
		{
			// pass the ids and let MY_Model delete the items
			$this->pyroscloud_m->delete_many($this->input->post('action_to'));
		}
		elseif (is_numeric($id))
		{
			// they just clicked the link so we'll delete that one
			$this->pyroscloud_m->delete($id);
		}
		redirect('admin/pyrosoundclouds');
	}
}
