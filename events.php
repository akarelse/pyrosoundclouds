<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Sample Events Class
 * 
 * @package    PyroCMS
 * @subpackage PyroSoundclouds
 * @category   events
 * @author     Aat Karelse
 * @website    http://vuurrosmedia.nl
 */
class Events_PyrosoundClouds {
		
		
		protected $ci;
		
		public function __construct()
		{
			$this->ci =& get_instance();
			Events::register('public_controller', array($this, 'run'));
		}
		
		public function run($template)
		{
				$this->ci->load->model('pyrosoundclouds/pyroscloud_m');
				$this->ci->pyroscloud_m->limit(5)->get_all();

		}

}
/* End of file events.php */
