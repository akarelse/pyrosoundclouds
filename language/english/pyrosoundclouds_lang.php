<?php
//messages
$lang['pyrosoundclouds:success']		=	'It worked';
$lang['pyrosoundclouds:error']			=	'It didn\'t work';
$lang['pyrosoundclouds:no_items']		=	'No Items';

//page titles
$lang['pyrosoundclouds:create']					=	'Create Item';

//labels
$lang['pyrosoundclouds:name']			=	'Name';
$lang['pyrosoundclouds:url']			=	'Number';
$lang['pyrosoundclouds:secrettoken']	=	'Secret Token';
$lang['pyrosoundclouds:manage']			=	'Manage';
$lang['pyrosoundclouds:item_list']		=	'Item List';
$lang['pyrosoundclouds:view']			=	'View';
$lang['pyrosoundclouds:edit']			=	'Edit';
$lang['pyrosoundclouds:delete']			=	'Delete';

$lang['Pyrosoundclouds:items']			=  	'Songs';
//buttons
$lang['pyrosoundclouds:custom_button']	=	'Custom Button';
$lang['pyrosoundclouds:items']			=	'Items';

$lang['pyrosoundclouds:urlexplanation']	=	'(the number you can find by pushing on the share button of a clip under the embed tab in the url for wordpress, it is the last nr at the end of the uri)'; 
?>
