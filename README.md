# PyroSoundCloud for PyroCMS

## Legal

This module was originally written by [Aat Karelse](http://vuurrosmedia.nl)
It is licensed under the version 3 of the GNU General Public License. [GPL license](http://www.gnu.org/licenses/)
More information about this module can be found at [more information](http://modules.vuurrosmedia.nl)

---

## Overview

PyroSoundCloud is a backend, frondend module for PyroCMS.
It supports version 2.1.x.
The module gives you the ability to add soundcloud musicto your site in the form of a widget with a list of sound cloud clips, also it has a plugin to add shortcodes directly in html.

---

## Features

- add soundcloud items.
- shortcode for soundcloud.
- widget

---

## Widgets

Widget for the list of soundclouds.

---

## Plugins

{{ soundclouds:cloudlist }} # list all the soundcloud tracks in your database
{{ soundclouds:cloud num="x"}} # get soundcloud from database at num x
{{ soundclouds:sound number="xyz" playertype="normal,tiny" backgroundcolor="#FF11AA" secrettoken="xyz"}} # get complete custom soundcloud track

---

## Settings

Comments (enable comments in the normal size player)
Buy (enable the buy button in the normal size player)
Show art work (show the art work in the normal size player)
Soundcloud url (dont change this if you are not sure)
Background color (could use some jquery ui color picker ?!?!)
Playertype (tiny and normal) (i want more types)

---

## Installation

Download the archive and upload via Control Panel.
Install the module.

---

## Credits

- Jerel Unruh for making available the sample module.
- The Pyro Dev team for making PyroCMS

---

## ToDo

+ adding a color picker (in the settings, kuddos to annyone adding js functionality to settings without altering core)

+ it is flash objects now, perhaps some javascript player ?
+ testing if it works for pyrocms 2.2.x
+ detailed bug testing
+ more atention for the plugins
+ see if most of template code could use plugins


