<div>
	<ul id="track-list">
	{{ if items_exist == false }}	
		<li>						
			<p>There are no items.</p>						   								
		</li>
		{{ else }}	
		{{ items }}
		<li>
			{{ if settings:playertype == "tiny" }}
			<object height="20px" width="100%">
			{{ else }}
			<object height="80px" width="100%">
			{{ endif}}
			{{ if secrettoken == '' }}
				<param name="movie" value="<?php echo $this->settings->soundcloudurl ?>{{url}}&amp;auto_play=false&amp;player_type=<?php echo $this->settings->playertype ?>&amp;font=Arial&amp;color=<?php echo $this->settings->backgroundcolor ?>">
				<param name="allowscriptaccess" value="always">
				<param name="wmode" value="transparent">
				<embed wmode="transparent" allowscriptaccess="always" height="<?php echo $this->settings->barheight ?>" src="<?php echo $this->settings->soundcloudurl ?>{{url}}&amp;auto_play=false&amp;player_type=<?php echo $this->settings->playertype ?>&amp;font=Arial&amp;color=<?php echo $this->settings->backgroundcolor ?>" type="application/x-shockwave-flash" width="100%">
			{{ else }}
				<param name="movie" value="<?php echo $this->settings->soundcloudurl ?>{{url}}?secret_token={{secrettoken}}&amp;auto_play=false&amp;player_type=<?php echo $this->settings->playertype ?>&amp;font=Arial&amp;color=<?php echo $this->settings->backgroundcolor ?>">
				<param name="allowscriptaccess" value="always">
				<param name="wmode" value="transparent">
				<embed wmode="transparent" allowscriptaccess="always" height="<?php echo $this->settings->barheight ?>" src="<?php echo $this->settings->soundcloudurl ?>{{url}}?secret_token={{secrettoken}}&amp;auto_play=false&amp;player_type=<?php echo $this->settings->playertype ?>&amp;font=Arial&amp;color=<?php echo $this->settings->backgroundcolor ?>" type="application/x-shockwave-flash" width="100%">
			{{ endif }}
			</object>
		</li>
		{{ /items }}
		{{ pagination:links }}	
	{{ endif }}
	</ul>
</div>
