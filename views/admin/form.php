
<section class="title">
	<!-- We'll use $this->method to switch between sample.create & sample.edit -->
	<h4><?php echo lang('pyrosoundclouds:'.$this->method); ?></h4>
</section>

<section class="item">
<div class="content">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		
		<div class="form_inputs">
	
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('pyrosoundclouds:name'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('name', set_value('name', $name), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="url"><?php echo lang('pyrosoundclouds:url'); ?> <span>*</span><?php echo lang('pyrosoundclouds:urlexplanation'); ?>  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAQAAACR313BAAAAaklEQVQY02NgIB0Yz8cv/R9JgXEDkPvf+L2xA5I0QgFYqgEkaRxgfB4sBYEJMOkGMJ0AFjwPNQ1JN0T6PswCVLth0u+NDbC4HCJtLAC1C10BTDeSZIHxf2RpkMsRsB/I/4/ub1TYwEB7AADai0fjk54O3AAAAABJRU5ErkJggg=="/> </label>
				<div class="input"><?php echo form_input('url', set_value('url', $url), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="secrettoken"><?php echo lang('pyrosoundclouds:secrettoken'); ?></label>
				<div class="input"><?php echo form_input('secrettoken', set_value('secrettoken', $secrettoken), 'class="width-15"'); ?></div>
			</li>
			
		</ul>
		
		</div>
		
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
		
	<?php echo form_close(); ?>
</div>
</section>

