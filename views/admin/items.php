<section class="title">
	<h4><?php echo lang('pyrosoundclouds:item_list'); ?></h4>
</section>

<section class="item">
	<div class="content">
	<?php echo form_open('admin/pyrosoundclouds/delete');?>
	
	<?php if (!empty($items)): ?>
	
		<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('pyrosoundclouds:name'); ?></th>
					<th><?php echo lang('pyrosoundclouds:url'); ?></th>
					<th><?php echo lang('pyrosoundclouds:secrettoken'); ?></th>
					<th></th>
				</tr>
			</thead>
			
			<tbody>
				<?php foreach( $items as $item ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
					<td><?php echo $item->name; ?></td>
					<td><?php echo $item->url; ?></td>
					<td><?php echo $item->secrettoken; ?></td>
					<td class="actions">
						<?php echo
						anchor('pyrosoundclouds/index/'.$item->url, lang('pyrosoundclouds:view'), 'class="btn green" target="_blank"').' '.
						anchor('admin/pyrosoundclouds/edit/'.$item->id, lang('pyrosoundclouds:edit'), 'class="btn orange"').' '?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('pyrosoundclouds:no_items'); ?></div>
	<?php endif;?>
	
	<?php echo form_close(); ?>
	</div>
</section>
