<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_pyrosoundclouds extends Module {

	public $version = '2.1';
	const MIN_PHP_VERSION = '5.3.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'PyrosoundClouds'
			),
			'description' => array(
				'en' => 'This is a PyroCMS PyrosoundClouds module.'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content', // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'sections' => array(
				'items' => array(
					'name' 	=> 'Pyrosoundclouds:items', // These are translated from your language file
					'uri' 	=> 'admin/pyrosoundclouds',
					'shortcuts' => array(
						'create' => array(
							'name' 	=> 'pyrosoundclouds:create',
							'uri' 	=> 'admin/pyrosoundclouds/create',
							'class' => 'add'
						)
					)
				)
			)
		);
	}

	public function install()
	{
		$this->uninstall();
		$this->check_php_version();
		$this->db->trans_start();

		$pyrocloudsdb = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'url' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'secrettoken' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
		);
						
		
		$bgcolor_setting = array(
			'slug' => 'backgroundcolor',
			'title' => 'Background color',
			'description' => 'Set the backgroundcolor of the soundcloud boxes',
			'`default`' => '0',
			'`value`' => 'FF0000',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'pyrosoundclouds'
		);
		
		$playertype_setting = array(
			'slug' => 'playertype',
			'title' => 'Player type',
			'description' => 'Set the type of the soundcloudplayer',
			'`default`' => '0',
			'`value`' => '0',
			'type' => 'select',
			'`options`' => 'tiny=Tiny|normal=Normal|normaljs=NormalJS',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'pyrosoundclouds'
		);
		
		$soundcloud_setting = array(
			'slug' => 'soundcloudurl',
			'title' => 'Sound Cloud URL',
			'description' => 'Set the sound cloud base url',
			'`default`' => 'https://player.soundcloud.com/player.swf?url=http://api.soundcloud.com/tracks/',
			'`value`' => 'https://player.soundcloud.com/player.swf?url=http://api.soundcloud.com/tracks/',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'pyrosoundclouds'
		);
		
		$artwork_setting = array(
			'slug' => 'showartwork',
			'title' => 'Show art work',
			'description' => 'Show the artwork or not',
			'`default`' => '0',
			'`value`' => '0',
			'type' => 'select',
			'`options`' => 'true=Yes|false=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'pyrosoundclouds'
		);
		
		$buyable_setting = array(
			'slug' => 'buyable',
			'title' => 'Buy',
			'description' => 'If tracks can be bought',
			'`default`' => '0',
			'`value`' => '0',
			'type' => 'select',
			'`options`' => 'true=Yes|false=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'pyrosoundclouds'
		);
		
		$sound_comments_setting = array(
			'slug' => 'sound_comments',
			'title' => 'Comments',
			'description' => 'Comments',
			'`default`' => '0',
			'`value`' => '0',
			'type' => 'select',
			'`options`' => 'true=Yes|false=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'pyrosoundclouds'
		);

		$this->db->insert('settings', $bgcolor_setting);
		$this->db->insert('settings', $playertype_setting);
		$this->db->insert('settings', $soundcloud_setting);
		$this->db->insert('settings', $artwork_setting);
		$this->db->insert('settings', $buyable_setting);
		$this->db->insert('settings', $sound_comments_setting);
	
		$this->dbforge->add_field($pyrocloudsdb);
		$this->dbforge->add_key('id', TRUE);

		$this->dbforge->create_table('pyrosoundclouds');
	
		$this->db->trans_complete();
		return $this->db->trans_status();
	}

	public function uninstall()
	{
		$this->dbforge->drop_table('pyrosoundclouds');
		$this->db->delete('settings', array('module' => 'pyrosoundclouds'));
		{
			return TRUE;
		}
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	/**
	 * Check the current version of PHP and thow an error if it's not good enough
	 *
	 * @access private
	 * @return boolean
	 * @author Victor Michnowicz
	 */
private function check_php_version()
	{
		// If current version of PHP is not up snuff
		if ( version_compare(PHP_VERSION, self::MIN_PHP_VERSION) < 0 )
		{
			show_error('This addon requires PHP version ' . self::MIN_PHP_VERSION . ' or higher.');
			return FALSE;
		}
			return TRUE;
	}
}
/* End of file details.php */
